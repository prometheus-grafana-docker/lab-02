# Prometheus and Grafana with Docker
# Lab 02: Create  docker-compose configuration filea and run the application


# Preparations
 - Verify that these files exist:
    - ~/prometheus-grafana/prometheus/prometheus.yml
    - ~/prometheus-grafana/grafana/grafana.ini 
    - ~/prometheus-grafana/grafana/datasource.yml
    
---

# Tasks

 - Create  docker-compose configuration file

 - Create containers using docker-compose 

 - run the applications


--- 
 
## Create  docker-compose configuration file

&nbsp;

- run command:

```
tee ~/prometheus-grafana/docker-compose.yml << EOF
version: "3.7"
services:
  prometheus:
    image: prom/prometheus:latest
    volumes:
      -  ~/prometheus-grafana/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
    ports:
      - 9090:9090
  grafana:
    image: grafana/grafana:7.5.17
    volumes:
      -  ~/prometheus-grafana/grafana/grafana.ini:/etc/grafana/grafana.ini
      -  ~/prometheus-grafana/grafana/datasource.yml:/etc/grafana/provisioning/datasources/datasource.yaml
    ports:
      - 3000:3000
    links:
      - prometheus
  node-exporter:
    image: prom/node-exporter:latest
    container_name: monitoring_node_exporter
    restart: unless-stopped
    expose:
      - 9100
EOF

```

## Create containers using docker-compose 

&nbsp;

- Navigate to data directory, run commands:

```

cd  ~/prometheus-grafana/

```

&nbsp;

- Run the applications with docker-compose, run commands:

```

docker compose up -d

```
<img alt="Image 1.1" src="images/verify_docker_compose.png" width="75%" height="75%">

&nbsp;

## Verify output:

- run commands:

```
 docker ps | grep grafana
```
<img alt="Image 1.1" src="images/verify_docker_compose_2.png" width="75%" height="75%">

&nbsp;

## run the applications

- Prometheus: open webbeowser and navigate to: http://serverip_or_hostname:9090 
- go to status-> targets, you should be able to see you targets

<img alt="Image 1.1" src="images/verify_prometheus.png" width="100%" height="100%">

- Grafana: open webbeowser and navigate to: http://serverip_or_hostname:3000 
- username: admin, password: admin

<img alt="Image 1.1" src="images/verify_grafana_1.png" width="100%" height="100%">
<img alt="Image 1.1" src="images/verify_grafana_2.png" width="100%" height="100%">
<img alt="Image 1.1" src="images/verify_grafana_3.png" width="100%" height="100%">
